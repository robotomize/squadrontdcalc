<?php

$enemy = [
    'l1' => [
        'name'          => 'Fat Zerlings',
        'attackDmg'     => 7.5,
        'health'        => 30,
        'attackType' => 'melee',
        'armor'         => 1.25,
        'attackSpeed'   => 1.25,
        'attackRange'   => 0,
        'attackDmgType' => 'piercing',
        'armorType'     => 'biological',
        'shields'       => 0,
        'cost'          => 0,
        'cellCnt'       => 1
    ]
];