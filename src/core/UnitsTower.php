<?php

namespace calc;

include 'BaseUnits.php';

class UnitsTower extends BaseUnits
{
    /**
     * @var string
     */
    protected $tier = '';

    /**
     * @var int
     */
    protected $buildCost = 0;

    /**
     * @var int
     */
    protected $totalCost = 0;

    /**
     * @var int
     */
    protected $CV = 0;

    /**
     * @var int
     */
    protected $energy = 0;

    /**
     * @var int
     */
    protected $energyRegen = 0;

    /**
     * @var float
     */
    protected $minDPS = 0.0;

    /**
     * @var float
     */
    protected $minHP = 0.0;

    /**
     * @return float
     */
    public function getMinDPS()
    {
        return $this->minDPS;
    }

    /**
     * @param float $minDPS
     */
    public function setMinDPS($minDPS)
    {
        $this->minDPS = $minDPS;
    }

    /**
     * @return float
     */
    public function getMinHP()
    {
        return $this->minHP;
    }

    /**
     * @param float $minHP
     */
    public function setMinHP($minHP)
    {
        $this->minHP = $minHP;
    }

    /**
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * @param string $tooltip
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    /**
     * @var string
     */
    protected $tooltip = '';

    /**
     * @return int
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * @param int $energy
     */
    public function setEnergy($energy)
    {
        $this->energy = (int)$energy;
    }

    /**
     * @return int
     */
    public function getEnergyRegen()
    {
        return $this->energyRegen;
    }

    /**
     * @param int $energyRegen
     */
    public function setEnergyRegen($energyRegen)
    {
        $this->energyRegen = (float)$energyRegen;
    }

    /**
     * @return int
     */
    public function getTotalBuildCost()
    {
        return $this->totalBuildCost;
    }

    /**
     * @param int $totalBuildCost
     */
    public function setTotalBuildCost($totalBuildCost)
    {
        $this->totalBuildCost = (int)$totalBuildCost;
    }

    /**
     * @var int
     */
    protected $totalBuildCost = 0;

    /**
     * @return int
     */
    public function getCV()
    {
        return $this->CV;
    }

    /**
     * @param int $CV
     */
    public function setCV($CV)
    {
        $this->CV = (int)$CV;
    }

    /**
     * @return string
     */
    public function getTier()
    {
        return $this->tier;
    }

    /**
     * @param string $tier
     */
    public function setTier($tier)
    {
        $this->tier = $tier;
    }

    /**
     * @return int
     */
    public function getBuildCost()
    {
        return $this->buildCost;
    }

    /**
     * @param int $buildCost
     */
    public function setBuildCost($buildCost)
    {
        $this->buildCost = (int)$buildCost;
    }

    /**
     * @return int
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * @param int $totalCost
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = (int)$totalCost;
    }

    /**
     * @return int
     */
    public function getTotalSupply()
    {
        return $this->totalSupply;
    }

    /**
     * @param int $totalSupply
     */
    public function setTotalSupply($totalSupply)
    {
        $this->totalSupply = (int)$totalSupply;
    }

    /**
     * @var int
     */
    protected $totalSupply = 0;

}