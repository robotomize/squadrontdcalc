<?php

namespace calc;

include 'EnemyUnitFactory.php';
include 'AbstractCalc.php';

/**
 * Class SquadronCalc
 * @package calc
 * @robotomize@gmail.com
 */
class SquadronCalc extends AbstractCalc
{

    /**
     * @var int
     */
    private $currentWave = 0;

    /**
     * @var int
     */
    private $waveCount = 1;

    /**
     * @return int
     */
    public function getWaveCount()
    {
        return $this->waveCount;
    }

    /**
     * @param int $waveCount
     */
    public function setWaveCount($waveCount)
    {
        $this->waveCount = $waveCount;
    }

    function __construct($currentWave, $count = 1)
    {
        if (empty($currentWave)) {
            throw new \Exception('Set current wave');
        } else {
            $this->currentWave = $currentWave;
            $this->waveCount = $count;
        }
    }

    /**
     * @return int
     */
    public function getCurrentWave()
    {
        return $this->currentWave;
    }

    /**
     * @param int $currentWave
     */
    public function setCurrentWave($currentWave)
    {
        $this->currentWave = $currentWave;
    }


    /**
     * @var array
     */
    private $currentLineInfo = [];

    private function viewCurrentLineInfo()
    {
        return '';
    }

    /**
     * @var array
     */
    private $waveEnemySet = [];

    public function viewPlayerSet()
    {
        foreach($this->playerTowerBuild as $values) {
            print sprintf('Name: %s', $values->name) . PHP_EOL;
            print sprintf('Tier: %s', $values->tier) . PHP_EOL;
            print sprintf('DPS: %s', $values->attackDPS) . PHP_EOL;
            print sprintf('Cost: %s', $values->cost) . PHP_EOL;
            print sprintf('Range: %s', $values->attackRange) . PHP_EOL;
            print sprintf('Attack type %s', $values->attackDmgType) . PHP_EOL;
        }
    }

    /**
     * @var array
     */
    private $playerTowerBuild = [];

    public function main()
    {
        $this->waveEnemySet = EnemyUnitFactory::makeUnits($this->waveCount, 'Zombie from Space', $this->currentWave);
        try {
            $this->summaryWaveHP();
        } catch(\Exception $e) {
            print 'Uncatchable error';
        }
    }

    private $avgWaveDmg = 0.0;

    /**
     * @return float
     */
    public function getAvgWaveDmg()
    {
        return $this->avgWaveDmg;
    }


    /**
     * @var int
     */
    private $summaryWaveHP = 0;

    /**
     * @return int
     */
    public function getSummaryWaveHP()
    {
        return $this->summaryWaveHP;
    }

    /**
     * @throws \Exception
     */
    private function summaryWaveHP()
    {
        if (!empty($this->waveEnemySet)) {
            foreach($this->waveEnemySet as $k => $v) {
                $this->summaryWaveHP = $this->summaryWaveHP + $v->getHealth();
                $this->avgWaveDmg = $this->avgWaveDmg + ($v->getAttackDmgMax() + $v->getAttackDmgMin()) / 2;
            }
        } else {
            throw new \Exception('create wave set of units');
        }
    }

    private function killOneUnitFromWave()
    {
        return false;
    }

    private function differenceDPS()
    {
        return false;
    }

    private function forecastCurrentWave()
    {
        return false;
    }

    private function chooseTower()
    {
        return false;
    }

    private function viewChanceCurrentWave()
    {
        return false;
    }
}

$tt = new SquadronCalc(10, 1);
$tt->main();
print $tt->getCurrentWave() . PHP_EOL;
print $tt->getSummaryWaveHP() . PHP_EOL;
print $tt->getAvgWaveDmg();