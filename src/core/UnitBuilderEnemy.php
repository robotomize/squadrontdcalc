<?php

namespace calc;

include 'IBuilder.php';
include 'UnitsEnemy.php';

/**
 * Class UnitBuilder
 * @package calc
 * @author robotomize@gmail.com
 * @version 0.0.0
 */
class UnitBuilderEnemy implements  IBuilder
{
    /**
     * @param $params
     * @return Units
     */
    public static function create($params)
    {
        $unit = new UnitsEnemy();
        try {
            $unit->massiveSetProperty($params);
            return $unit;
        } catch(\Exception $e) {
            print_r($e);
            return $unit;
        }
    }
}