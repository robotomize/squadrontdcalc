<?php

namespace calc;

/**
 * Class Ability
 * @package calc
 * @author robotomize@gmail.com
 */
class Ability
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var array
     */
    private $modificationProperty = [];

    /**
     * @param $property
     */
    private function modificationPlus($property)
    {
    }

    /**
     * @param $property
     */
    private function modificationMinus($property)
    {

    }
}