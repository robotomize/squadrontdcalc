<?php

namespace calc;

include 'DataParser.php';
include 'IFactory.php';
include 'UnitBuilderEnemy.php';

class EnemyUnitFactory implements IFactory
{
    /**
     * @var array
     */
    private static $enemyArray = [];

    /**
     * @param $name
     * @return array
     */
    public static function fetchData($name, $wave = 0)
    {
        $tt = new DataParser('Waves.csv', 'EnemyTranslator');
        self::$enemyArray = $tt->getCharacteristic();

        foreach(self::$enemyArray as $kk => $vv) {
            if ($wave != 0) {
                if ($vv['wave'] == $wave) {
                    return self::$enemyArray[$kk];
                }
            } else {
                if (array_search($name, $vv, false)) {
                    return self::$enemyArray[$kk];
                }
            }

        }
        return [];
    }

    /**
     * @param int $cnt
     * @param $name
     * @param $wave
     * @return array
     */
    public static function makeUnits($cnt = 10, $name, $wave)
    {
        $arrayObjects = [];

        for($i = 0; $i < $cnt; $i++) {
            $arrayObjects[] = UnitBuilderEnemy::create(self::fetchData($name, $wave));
        }

        return $arrayObjects;
    }
}

//print_r(EnemyUnitFactory::makeUnits(15, 'Zombie from Space', 3));

//$tt = new EnemyUnitFactory();
//print $tt->fetchData();
//$tt->makeUnits(1, 'zerg');