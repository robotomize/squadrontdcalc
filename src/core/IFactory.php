<?php

namespace calc;

/**
 * Interface IFactory
 * @package calc
 */
interface IFactory
{
    public static function makeUnits($cnt, $name, $wave);
    public static function fetchData($name, $wave);
}