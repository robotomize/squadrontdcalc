<?php

namespace calc;

include 'BaseUnits.php';

/**
 * Class UnitsEnemy
 * @package calc
 * @author robotomize@gmail.com
 */
class UnitsEnemy extends BaseUnits
{
    /**
     * @return int
     */
    public function getArmor()
    {
        return $this->armor;
    }

    /**
     * @param int $armor
     */
    public function setArmor($armor)
    {
        $this->armor = (float)$armor;
    }

    /**
     * @var int
     */
    protected $armor = 0;

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = (int)$count;
    }

    /**
     * @var int
     */
    protected $incomeTotal = 0;

    /**
     * @return int
     */
    public function getIncomeTotal()
    {
        return $this->incomeTotal;
    }

    /**
     * @param int $incomeTotal
     */
    public function setIncomeTotal($incomeTotal)
    {
        $this->incomeTotal = (int)$incomeTotal;
    }

    /**
     * @var int
     */
    protected $wave = 1;

    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @return int
     */
    public function getWave()
    {
        return $this->wave;
    }

    /**
     * @param int $wave
     */
    public function setWave($wave)
    {
        $this->wave = (int)$wave;
    }


}
