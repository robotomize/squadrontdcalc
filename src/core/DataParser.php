<?php

namespace calc;

use League\Csv\Reader;

require '../../vendor/autoload.php';
require_once 'EnemyTranslator.php';
require_once 'TowerTranslator.php';

/**
 * Class DataParser
 * @package calc
 * @author robotomize@gmail.com
 * @version 0.1
 * @TODO connect monolog && log errs
 */
class DataParser extends Reader
{

    /**
     * @var string
     */
    private $fileName = '';

    /**
     * @return array
     */
    public function getCharacteristicKeys()
    {
        return $this->characteristicKeys;
    }

    /**
     * @var array
     */
    private $characteristicKeys = [];

    /**
     * @var array
     */
    private $characteristic = [];

    /**
     * @return array
     */
    public function getCharacteristic()
    {
        return $this->characteristic;
    }

    private $translatorClass = '';

    /**
     * @param string $translatorClass
     */
    public function setTranslatorClass($translatorClass)
    {
        $this->translatorClass = $translatorClass;
    }

    /**
     * @param object|string $filename
     * @throws \Exception
     */
    public function __construct($filename, $translator)
    {
        if (empty($filename) || empty($translator)) {
            throw new \InvalidArgumentException('filename is empty');
        }

        /**
        if (class_exists($translator)) {
            $this->translatorClass = $translator;
        } else {
            throw new \Exception('Translator Class load error');
        }
        */

        $this->translatorClass = $translator;
        $this->fileName = __DIR__ . '/../' . 'data/' . $filename;
        if (!file_exists($this->fileName)) {
            throw new \Exception('File ' . $this->fileName . ' not found');
        } else {
            $this->parse();
        }
    }

    /**
     * @var array
     */
    private $tempCharKeys = [];

    /**
     * Main parse method
     */
    private function parse()
    {
        $inputCsv = Reader::createFromPath($this->fileName);
        $inputCsv->setDelimiter(';');

        /**
         * get keys from first line
         */
        $this->characteristicKeys = $inputCsv->fetchOne();
        try {
            switch($this->translatorClass) {
                case 'TowerTranslator':
                    $enemyTrans = new TowerTranslator($this->characteristicKeys);
                    break;
                case 'EnemyTranslator':
                    $enemyTrans = new EnemyTranslator($this->characteristicKeys);
                    break;
                default:
                    $enemyTrans = new EnemyTranslator($this->characteristicKeys);
            }

        $this->characteristicKeys = $enemyTrans->getCharacteristicKeys();
        } catch(\Exception $e) {
            print_r($e);
        }

        /**
         * get other lines from file to array
         */
        $this->characteristic = $inputCsv->fetchAssoc($this->characteristicKeys);
        $this->badDataFilter();
    }

    /**
     * @return array
     */
    public function getTempCharKeys()
    {
        return $this->tempCharKeys;
    }

    /**
     * 
     */
    private function badDataFilter()
    {
        foreach($this->characteristic as $key => $value) {
            foreach($value as $kk => $vv) {
                switch ($kk) {
                    /*
                    case 'armor':
                        try {
                            floatval($vv);
                        } finally {
                            $m = explode('(B)', $vv);
                            $this->tempCharKeys[$kk] = (!empty($m[1]) ? $m[1] : $m[0]);
                        }

                        break;
                    */
                    case 'attackSpeed':
                        $this->tempCharKeys[$key][$kk] = str_replace(",", ".", $vv);
                        break;
                    case 'attackRange':
                        $this->tempCharKeys[$key][$kk] = str_replace(",", ".", $vv);
                        break;
                    case 'attackDPS':
                        $this->tempCharKeys[$key][$kk] = str_replace(",", ".", $vv);
                        break;
                    default:
                        $this->tempCharKeys[$key][$kk] = $vv;
                        break;
                }
            }
        }
        $this->characteristic = $this->tempCharKeys;
    }

    /**
     * @return string
     */
    public function __invoke()
    {
        if (!empty($this->characteristic)) {
            return print_r($this->characteristic);
        } else {
            return 'Characteristic array is empty';
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (!empty($this->characteristic)) {
            return serialize($this->characteristic);
        } else {
            return 'Characteristic array is empty';
        }
    }
}

//$tt = new DataParser('Towers.csv', 'TowerTranslator');
//print_r($tt->getCharacteristic());
