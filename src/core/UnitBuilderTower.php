<?php

namespace calc;

include 'IBuilder.php';
include 'UnitsTower.php';

/**
 * Class UnitBuilder
 * @package calc
 * @author robotomize@gmail.com
 * @version 0.0.0
 */
class UnitBuilderTower implements  IBuilder
{
    /**
     * @param $params
     * @return UnitsTower
     */
    public static function create($params)
    {
        $unit = new UnitsTower();
        try {
            $unit->massiveSetProperty($params);
            return $unit;
        } catch(\Exception $e) {
            print_r($e);
            return $unit;
        }
    }
}