<?php

namespace calc;

/**
 * Class AbstractTranslator
 * @package calc
 */
abstract class AbstractTranslator
{
    abstract protected function translator();
}