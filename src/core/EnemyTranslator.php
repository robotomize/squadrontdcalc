<?php

namespace calc;

require_once 'AbstractTranslator.php';
/**
 * Class EnemyTranslator
 * @package calc
 * @author robotomize@gmail.com
 */
class EnemyTranslator extends AbstractTranslator
{
    /**
     * @var array
     */
    private $replaceKeys = [
        'Wave'                  => 'wave',
        'Name'                  => 'name',
        'Number of Mobs'        => 'count',
        'Bounty'                => 'bounty',
        'Wave Bonus'            => 'waveBonus',
        'Total Wave Income'     => 'incomeTotal',
        'Aggr. Income'          => 'aggrIncome',
        'HP'                    => 'health',
        'Dmg. min'              => 'attackDmgMin',
        'Dmg. max'              => 'attackDmgMax',
        'Weapon Speed'          => 'attackSpeed',
        'Range'                 => 'attackRange',
        'Dmg. Type'             => 'attackDmgType',
        'Armor'                 => 'armor',
        'Armor Type'            => 'armorType',
        'XP'                    => 'xp',
        'Extra info'            => 'extraInfo',
        'Wave DPS'              => 'attackDPS',
        'Wave effective HP'     => 'waveEffecHp',
        'SS focus priority'     => 'ssFocusPrior'
    ];

    /**
     * @var array
     */
    private $tempCharKeys = [];

    /**
     * @return array
     */
    public function getCharacteristicKeys()
    {
        return $this->characteristicKeys;
    }

    /**
     * @var array
     */
    private $characteristicKeys = [];

    /**
     * @param $params
     */
    function __construct($params)
    {
        $this->characteristicKeys = $params;
        $this->translator();
    }

    /**
     * @return array
     */
    function __invoke()
    {
        if (!empty($this->characteristicKeys)) {
            return $this->characteristicKeys;
        } else {
            return [];
        }
    }

    /**
     * replace csv fields to Class fields
     */
    protected function translator()
    {
        foreach($this->characteristicKeys as $kk => $vv) {
            foreach($this->replaceKeys as $key => $value) {
                if ($vv == $key) {
                    $this->tempCharKeys[] = $value;
                }
            }
        }
        $this->characteristicKeys = $this->tempCharKeys;
        unset($this->tempCharKeys);
    }
}