<?php

namespace calc;

/**
 * Class AbstractUnits
 * @package calc
 * @author robotomize@gmail.com
 * @version 0.0.2
 * @usage
 */
class BaseUnits
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var int
     */
    protected $movementSpeed = 0;

    /**
     * @var int
     */
    protected $attackSpeed = 0;

    /**
     * @var float
     */
    protected $attackRange = 0.0;

    /**
     * @var int
     */
    protected $cellCnt = 0;

    /**
     * @var int
     */
    protected $attackDmgMin = 0;

    /**
     * @return int
     */
    public function getAttackDmgMin()
    {
        return $this->attackDmgMin;
    }

    /**
     * @param int $attackDmgMin
     */
    public function setAttackDmgMin($attackDmgMin)
    {
        $this->attackDmgMin = (float)$attackDmgMin;
    }

    /**
     * @return int
     */
    public function getAttackDmgMax()
    {
        return $this->attackDmgMax;
    }

    /**
     * @param int $attackDmgMax
     */
    public function setAttackDmgMax($attackDmgMax)
    {
        $this->attackDmgMax = (float)$attackDmgMax;
    }

    /**
     * @return int
     */
    public function getAttackDPS()
    {
        return $this->attackDPS;
    }

    /**
     * @param int $attackDPS
     */
    public function setAttackDPS($attackDPS)
    {
        $this->attackDPS = (float)$attackDPS;
    }

    /**
     * @return int
     */
    public function getAttackCount()
    {
        return $this->attackCount;
    }

    /**
     * @param int $attackCount
     */
    public function setAttackCount($attackCount)
    {
        $this->attackCount = (int)$attackCount;
    }

    /**
     * @var int
     */
    protected $attackDmgMax = 0;

    /**
     * @var int
     */
    protected $attackDPS = 0;

    /**
     * @var int
     */
    protected $attackCount = 0;

    /**
     * @var array
     */
    protected $abilities = [];

    /**
     * @var int
     */
    protected $health = 0;

    /**
     * @var int
     */
    protected $shields = 0;

    /**
     * @var string
     */
    protected $attackDmgType = '';

    /**
     * @var string
     */
    protected $armorType = '';

    /**
     * @return string
     */
    public function getArmorType()
    {
        return $this->armorType;
    }

    /**
     * @param string $armorType
     */
    public function setArmorType($armorType)
    {
        $this->armorType = $armorType;
    }

    /**
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * @param string $extraInfo
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;
    }

    /**
     * @var string
     */
    protected $extraInfo = '';

    /**
     * @return string
     */
    public function getAttackType()
    {
        return $this->attackType;
    }

    /**
     * @param string $attackType
     */
    public function setAttackType($attackType)
    {
        $this->attackType = $attackType;
    }

    /**
     * @var string
     */
    protected $attackType = '';

    /**
     * @var int
     */
    protected $cost = 0;

    /**
     * @param int $cost
     */
    public function setCost($cost)
    {
        $this->cost = (int)$cost;
    }

    /**
     * @param string $attackDmgType
     */
    public function setAttackDmgType($attackDmgType)
    {
        $this->attackDmgType = $attackDmgType;
    }

    /**
     * @param int $shields
     */
    public function setShields($shields)
    {
        $this->shields = (int)$shields;
    }

    /**
     * @param int $health
     */
    public function setHealth($health)
    {
        $this->health = (int)$health;
    }

    /**
     * @param array $abilities
     */
    public function setAbilities($abilities)
    {
        $this->abilities = $abilities;
    }

    /**
     * @param int $attackDmg
     */
    public function setAttackDmg($attackDmg)
    {
        $this->attackDmg = (float)$attackDmg;
    }

    /**
     * @param int $cellCnt
     */
    public function setCellCnt($cellCnt)
    {
        $this->cellCnt = (int)$cellCnt;
    }

    /**
     * @param float $attackRange
     */
    public function setAttackRange($attackRange)
    {
        $this->attackRange = $attackRange;
    }

    /**
     * @param int $attackSpeed
     */
    public function setAttackSpeed($attackSpeed)
    {
        $this->attackSpeed = (float)$attackSpeed;
    }

    /**
     * @param int $movementSpeed
     */
    public function setMovementSpeed($movementSpeed)
    {
        $this->movementSpeed = (int)$movementSpeed;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getMovementSpeed()
    {
        return $this->movementSpeed;
    }

    /**
     * @return int
     */
    public function getAttackSpeed()
    {
        return $this->attackSpeed;
    }

    /**
     * @return float
     */
    public function getAttackRange()
    {
        return $this->attackRange;
    }

    /**
     * @return int
     */
    public function getCellCnt()
    {
        return $this->cellCnt;
    }

    /**
     * @return int
     */
    public function getAttackDmg()
    {
        return $this->attackDmg;
    }

    /**
     * @return array
     */
    public function getAbilities()
    {
        return $this->abilities;
    }

    /**
     * @return int
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @return int
     */
    public function getShields()
    {
        return $this->shields;
    }

    /**
     * @return string
     */
    public function getAttackDmgType()
    {
        return $this->attackDmgType;
    }

    /**
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param $params
     */
    public function massiveSetProperty($params)
    {
        foreach($params as $kk => $vv) {
            $this->{$kk} = $vv;
        }
    }

    /**
     * @var array
     */
    private $unavailableProperty = [];

    /**
     * @return array
     */
    public function getUnavailableProperty()
    {
        return $this->unavailableProperty;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->unavailableProperty[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->unavailableProperty)) {
            return $this->unavailableProperty[$name];
        } else {
            return false;
        }
    }

}