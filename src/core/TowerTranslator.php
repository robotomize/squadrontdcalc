<?php

namespace calc;

require_once 'AbstractTranslator.php';

/**
 * Class TowerTranslator
 * @package calc
 * @author robotomize@gmail.com
 */
class TowerTranslator extends AbstractTranslator
{
    /**
     * @var array
     */
    private $replaceKeys = [
        'Tier'                                                          => 'tier',
        'Name'                                                          => 'name',
        'CV'                                                            => 'CV',
        'Build Cost'                                                    => 'cost',
        'Total Cost'                                                   => 'totalBuildCost',
        'Total Supply'                                                  => 'totalSupply',
        'Shield'                                                        => 'shields',
        'HP'                                                            => 'health',
        'Armor Type'                                                    => 'armorType',
        'Dmg. Type'                                                     => 'attackDmgType',
        'Dmg. min'                                                      => 'attackDmgMin',
        'Dmg. max'                                                      => 'attackDmgMax',
        'Weapon Speed'                                                  => 'attackSpeed',
        'Attack Count'                                                  => 'attackCount',
        'DPS'                                                           => 'attackDPS',
        'DPS/ (10Min)'                                                  => 'minDPS',
        'HP/ (10Min)'                                                   => 'minHP',
        'Range'                                                         => 'attackRange',
        'Energy'                                                        => 'energy',
        'Energy Regen'                                                  => 'energyRegen',
        'Ability tooltips (with some non-informational text stripped)'  => 'tooltip',
        'Extra info'                                                    => 'extraInfo'
    ];

    /**
     * @var array
     */
    private $tempCharKeys = [];

    /**
     * @return array
     */
    public function getCharacteristicKeys()
    {
        return $this->characteristicKeys;
    }

    /**
     * @var array
     */
    private $characteristicKeys = [];

    /**
     * @param $params
     */
    function __construct($params)
    {
        $this->characteristicKeys = $params;
        $this->translator();
    }

    /**
     * @return array
     */
    function __invoke()
    {
        if (!empty($this->characteristicKeys)) {
            return $this->characteristicKeys;
        } else {
            return [];
        }
    }

    /**
     * replace csv fields to Class fields
     */
    protected function translator()
    {
        foreach($this->characteristicKeys as $kk => $vv) {
            foreach($this->replaceKeys as $key => $value) {
                if ($vv == $key) {
                    $this->tempCharKeys[] = $value;
                }
            }
        }
        $this->characteristicKeys = $this->tempCharKeys;
        unset($this->tempCharKeys);
    }
}