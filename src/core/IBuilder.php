<?php

namespace calc;

/**
 * Interface IBuilder
 * @package calc
 */
interface IBuilder
{
    public static function create($params);
}