<?php

namespace calc;

include 'DataParser.php';
include 'IFactory.php';
include 'UnitBuilderTower.php';

class TowerUnitFactory implements IFactory
{
    /**
     * @var array
     */
    private static $towerArray = [];

    /**
     * @param $name
     * @return array
     */
    public static function fetchData($name, $wave = 0)
    {
        $tt = new DataParser('Towers.csv', 'TowerTranslator');
        self::$towerArray = $tt->getCharacteristic();

        foreach(self::$towerArray as $kk => $vv) {
            if (array_search($name, $vv, false)) {
                return self::$towerArray[$kk];
            }
        }
        return [];
    }

    /**
     * @param int $cnt
     * @param $name
     * @param $wave
     * @return array
     */
    public static function makeUnits($cnt = 10, $name, $wave)
    {
        $arrayObjects = [];

        for($i = 0; $i < $cnt; $i++) {
            $arrayObjects[] = UnitBuilderTower::create(self::fetchData($name, $wave));
        }

        return $arrayObjects;
    }
}

//print_r(TowerUnitFactory::makeUnits(15, 'SPRITE', 3));

//$tt = new EnemyUnitFactory();
//print $tt->fetchData();
//$tt->makeUnits(1, 'zerg');